package com.example.nikolay.kodekinopoiskstreluk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.nikolay.kodekinopoiskstreluk.MyClasess.Genre;
import com.example.nikolay.kodekinopoiskstreluk.MyClasess.ParseTask;
import com.example.nikolay.kodekinopoiskstreluk.MyClasess.ParseTaskLevel2;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends Activity {

    ListView lvMain;
    ArrayList<Genre> ArrGenres = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lvMain = (ListView) findViewById(R.id.lvMain);
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
             //   Toast.makeText(getApplicationContext(), Integer.toString(Genres.get((int)id).id), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(),Films.class);
                intent.putExtra("Id",Integer.toString(ArrGenres.get((int)id).id));
                intent.putExtra("Name",ArrGenres.get((int)id).name);
                startActivity(intent);
            }
        });
        new ParseTaskLevel2("http://api.kinopoisk.cf/getGenres",lvMain,"genreData","genreName","genreID",getBaseContext(),ArrGenres).execute();
    }


}
