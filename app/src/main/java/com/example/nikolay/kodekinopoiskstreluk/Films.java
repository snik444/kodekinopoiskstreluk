package com.example.nikolay.kodekinopoiskstreluk;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;


import com.example.nikolay.kodekinopoiskstreluk.MyClasess.Genre;
import com.example.nikolay.kodekinopoiskstreluk.MyClasess.ParseTaskLevel2;



import java.util.ArrayList;

public class Films extends AppCompatActivity {

    ArrayList<Genre> ArrFilms = new ArrayList<>();
   ListView lvMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_films);
        lvMain = (ListView) findViewById(R.id.lvMain1);
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(getApplicationContext(),Film.class);
                intent.putExtra("Id",Integer.toString( ArrFilms.get((int)id).id));
                intent.putExtra("Name", ArrFilms.get((int)id).name);
                startActivity(intent);

            }});
        //   Toast.makeText(getApplicationContext(), getIntent().getStringExtra("Id") , Toast.LENGTH_SHORT).show();
        TextView  TV = (TextView)findViewById(R.id.Text3);
        TV.setText(getResources().getString(R.string.janre)+": "+getIntent().getStringExtra("Name"));
        new ParseTaskLevel2("http://api.kinopoisk.cf/getTodayFilms?genreID="+getIntent().getStringExtra("Id"),lvMain,"filmsData","nameRU","id",getBaseContext(),ArrFilms).execute();
    }

}
