package com.example.nikolay.kodekinopoiskstreluk.MyClasess;

/**
 * Created by Nikolas on 20.10.2016.
 */

public class Genre {
    public String name;
    public Integer id;
    public Genre(String name, Integer id)
    {
        this.id=id;
        this.name=name;
    }
}
