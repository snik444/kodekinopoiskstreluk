package com.example.nikolay.kodekinopoiskstreluk.MyClasess;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Nikolas on 20.10.2016.
 */

public  class ParseTaskLevel2 extends ParseTask
{
    ListView lvParseTask;
    String Jmain, Jname, JId;
    ArrayList<Genre> Genres;
    Context context;

    public ParseTaskLevel2(String Url, ListView lvParseTask, String Jmain, String Jname, String JId, Context context, ArrayList<Genre> Genres) {
        super(Url);
        this.lvParseTask = lvParseTask;
        this.Jmain=Jmain;
        this.Jname=Jname;
        this.JId=JId;
        this.Genres=Genres;
        this.context=context;

    }

    @Override
    protected void onPostExecute(String strJson) {
        super.onPostExecute(strJson);
        // выводим целиком полученную json-строку
        //      Log.d(LOG_TAG, strJson);
        ArrayList<String> names = new ArrayList<String>();
        JSONObject dataJsonObj = null;
        String secondName = "";
        try {

            dataJsonObj = new JSONObject(strJson);
            JSONArray genreData = dataJsonObj.getJSONArray(Jmain);
            JSONObject secondFriend;
            Genres.clear();

            for(int i=0;i<genreData.length();i++) {
                secondFriend = genreData.getJSONObject(i);
                //  Toast.makeText(getApplicationContext(), secondFriend.getString("genreName"), Toast.LENGTH_SHORT).show();
                Genres.add(new Genre(secondFriend.getString(Jname),secondFriend.getInt(JId)));
                names.add(secondFriend.getString(Jname));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, names);
        lvParseTask.setAdapter(adapter);
    }
}
