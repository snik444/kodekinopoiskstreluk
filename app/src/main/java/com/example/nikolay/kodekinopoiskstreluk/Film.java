package com.example.nikolay.kodekinopoiskstreluk;


import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;

import android.widget.TextView;


import com.example.nikolay.kodekinopoiskstreluk.MyClasess.DownloadImageTask;

import com.example.nikolay.kodekinopoiskstreluk.MyClasess.ParseTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Film extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film);
        TextView TV = (TextView) findViewById(R.id.textView2);
        TV.setText(getResources().getString(R.string.film) + ": " + getIntent().getStringExtra("Name"));
        String id=getIntent().getStringExtra("Id");
        new DownloadImageTask((ImageView) findViewById(R.id.imageView)).execute("https://st.kp.yandex.net/images/film_big/"+id+".jpg");
        new MyParseTask("http://api.kinopoisk.cf/getSeance?filmID="+id,(TextView) findViewById(R.id.textView)).execute();
        TextView TV4 = (TextView) findViewById(R.id.textView4);
        TV4.setMovementMethod(LinkMovementMethod.getInstance());
        new MyParseTaskSeanse("http://api.kinopoisk.cf/getSeance?filmID="+id+"&cityID=490",TV4).execute();

    }



    public String Val(String l, String v)
    {
        if(v.length()!=0)
            return "<p><b>"+l+":</b> <i>"+v+"</i></p>";
        return "";
    }

    public class MyParseTask extends ParseTask {
        TextView textView;

        public MyParseTask(String Url, TextView textView) {
            super(Url);
            this.textView=textView;

        }
        @Override
        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);
            JSONObject dataJsonObj = null;
            try {
                dataJsonObj = new JSONObject(strJson);
                String S="<br>";
                S+=Val("Название на английском",dataJsonObj.getString("nameEN"));
                S+=Val("Год",dataJsonObj.getString("year"));
                S+=Val("Продолжительность",dataJsonObj.getString("filmLength"));
                S+=Val("Страна",dataJsonObj.getString("country"));
                S+=Val("Жанры",dataJsonObj.getString("genre"));
                textView.setText((Html.fromHtml(S)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public class MyParseTaskSeanse extends ParseTask {
        TextView textView;

        public MyParseTaskSeanse(String Url, TextView textView) {
            super(Url);
            this.textView=textView;
        }
        @Override
        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);
            JSONObject dataJsonObj = null;
            try {
                dataJsonObj = new JSONObject(strJson);
                String S="<br>";
                S+=Val("Название города",dataJsonObj.getString("cityName"));
                JSONArray arr = dataJsonObj.getJSONArray("items");
                for (int i = 0; i < arr.length(); i++)
                {
                    S+=Val("\tКинотеатр",arr.getJSONObject(i).getString("cinemaName"));
                    S+=Val("\tАдресс",arr.getJSONObject(i).getString("address"));
                    String Lat=arr.getJSONObject(i).getString("lat");
                    String Lot =arr.getJSONObject(i).getString("lon");
                    S+="\t <a href=\"https://www.google.com.ua/maps/@"+Lat+","+Lot+",20z\">Местоположения кинотеатра</a>";
                    JSONArray msg = (JSONArray) arr.getJSONObject(i).get("seance");
                    for (int j = 0; j < msg.length(); j++)
                    S+=Val("\t\tСеанс",msg.getJSONObject(j).getString("time"));
                }
                textView.setText((Html.fromHtml(S)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
}